// Third-party modules
var express = require('express');
var winston = require('winston');
var cfg = require('config');

// Local modules
var notifier = require('./lib/notifier');
var model = require('./lib/model');

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, { timestamp: true });

var app = express();

var argv = require('yargs')
        .default('port', 9999)
        .argv;

app.set('views', cfg.views_dir);
app.set('view engine', 'jade');

app.use('/static', express.static(__dirname + '/static'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use(require('body-parser').json());

app.get('/', function(req, res){
    res.render('main');
});

app.get('/mustache', function(req, res){
    res.render('mustache');
});

app.get('/clients', function(req, res){
    model.Client.find({}, function(err, clients){
        res.json(clients);
    });
});

app.get('/clients/:id', function(req, res){
    model.Client.findOne({ _id: req.params.id }, function(err, c){
        res.json(c);
    });
});

app.get('/clients/delete_all', function(req, res){
    model.Client.find({}, function(err, clients){
        for (var i = 0; i < clients.length; i++){
            clients[i].remove();
        }

        res.send();
    });
});

app.delete('/clients/:id', function(req, res){
    model.Client.findByIdAndRemove(req.params.id, function(err, c){
        res.json(c);
    });
});

app.post('/clients', function(req, res){
    var c = new model.Client(req.body);

    c.save(function(err){ 
       res.json(c);
    });
});

app.put('/client/:id', function(req, res){
    model.Client.findByIdAndUpdate(req.params.id, req.body, function(err, c){
        res.json(c);
    });
});

winston.info('Listening on port %d', argv.port);
app.listen(argv.port);
