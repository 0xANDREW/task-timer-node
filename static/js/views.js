var MainRouter = Backbone.Router.extend({
    routes: {
        '': 'show_main',
        'clients': 'show_clients',
        'clients/add': 'add_client',
        'clients/:id/edit': 'edit_client',
        'clients/:id/projects': 'show_projects',
        'clients/:id/projects/add': 'add_project'
    },

    initialize: function(opts){
        this.active_view = null;
        this.$el = $(opts.el);
    },

    load_view: function(view){
        if (this.active_view){
            if (this.active_view.teardown){
                this.active_view.teardown();
            }

            this.active_view.remove();
        }

        this.active_view = view;
        this.$el.html(this.active_view.render());

        if (this.active_view.post_render){
            this.active_view.post_render();
        }
    },

    show_main: function(){
        this.$el.html($.Mustache.render('main'));
    },

    show_clients: function(){
        this.load_view(new ClientListView());
    },

    add_client: function(){
        this.load_view(new AddClientView());

        this.listenTo(this.active_view, 'saved', function(){
            this.navigate('clients', { trigger: true });
        });
    },

    edit_client: function(id){
        this.load_view(new EditClientView({ client_id: id }));
    },

    show_projects: function(id){
        this.load_view(new ProjectListView({ client_id: id }));
    },

    add_project: function(id){
        this.load_view(new AddProjectView({ client_id: id }));

        this.listenTo(this.active_view, 'saved', function(){
            var path = _.string.sprintf('clients/%s/projects', id);

            this.navigate(path, { trigger: true });
        });
    }
});

var ClientListView = Backbone.View.extend({
    events: {
        'click #delete_all': 'delete_all',
        'click .delete': 'delete'
    },

    initialize: function(){
        this.clients = new ClientCollection();
        this.load_clients();
    },

    load_clients: function(){
        this.clients.fetch().done(this.render.bind(this));
    },

    render: function(){
        var m = this.clients.map(function(c){ return c.attributes; });

        return this.$el.html($.Mustache.render('clients', { clients: m }));
    },

    delete_all: function(){
        $.get('/clients/delete_all').done(this.load_clients.bind(this));
    },

    delete: function(ev){
        var id = $(ev.currentTarget).parents('.client-row').attr('id').split('_')[1];

        this.clients.get(id).destroy().done(this.render.bind(this));
    }
});

var AddClientView = Backbone.View.extend({
    events: {
        'click #save': 'save'
    },

    render: function(){
        return this.$el.html($.Mustache.render('add_client'));
    },

    save: function(){
        var client = new Client();

        _.each(this.$('.form-control'), function(el){
            client.set(el.id, $(el).val());
        });

        client.save().done(function(){
            this.trigger('saved');
        }.bind(this));
    }
});

var EditClientView = Backbone.View.extend({
    events: {
        'click #save': 'save'
    },

    initialize: function(opts){
        this.model = new Client({ _id: opts.client_id });
        this.model.fetch().done(this.render.bind(this));
    },

    render: function(){
        this.$el.html($.Mustache.render('add_client'));

        _.each(this.$('.form-control'), function(el){
            $(el).val(this.model.get(el.id));
        }.bind(this));

        return this.$el;
    },

    save: function(){
        _.each(this.$('.form-control'), function(el){
            this.model.set(el.id, $(el).val());
        }.bind(this));

        this.model.save().done(function(){
            ROUTER.navigate('clients', { trigger: true });
        }.bind(this));
    }
});

var ProjectListView = Backbone.View.extend({
    events: {
        'click .delete': 'delete_project',
        'click #delete_all': 'delete_all'
    },

    initialize: function(opts){
        this.model = new Client({ _id: opts.client_id });
        this.model.fetch().done(this.render.bind(this));
    },

    render: function(){
        return this.$el.html($.Mustache.render('projects', {
            client_id: this.model.id,
            projects: this.model.get('projects')
        }));
    },

    delete_project: function(ev){
        var id = $(ev.currentTarget).parents('.project-row').attr('id').split('_')[1];

        var projects = _.reject(this.model.get('projects'), function(p){
            return p.id == id;
        });

        this.model.set('projects', projects);
        this.model.save().done(this.render.bind(this));
    },

    delete_all: function(){
        this.model.set('projects', []);
        this.model.save().done(this.render.bind(this));
    }
});

var AddProjectView = Backbone.View.extend({
    events: {
        'click #save': 'save'
    },

    initialize: function(opts){
        this.model = new Client({ _id: opts.client_id });
        this.model.fetch().done(this.render.bind(this));
    },

    render: function(){
        return this.$el.html($.Mustache.render('add_project', { client_id: this.model.id }));
    },

    save: function(){
        var project = {
            id: _.uniqueId(),
            tasks: []
        };

        _.each(this.$('.form-control'), function(el){
            project[el.id] = $(el).val();
        });

        this.model.get('projects').push(project);

        this.model.save().done(function(){
            this.trigger('saved');
        }.bind(this));
    }
});
