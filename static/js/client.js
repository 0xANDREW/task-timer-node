var Client = Backbone.Model.extend({
    urlRoot: '/clients',
    idAttribute: '_id',
    defaults: {
        projects: []
    }
});

var ClientCollection = Backbone.Collection.extend({
    model: Client,
    url: '/clients'
});
