var ROUTER;

$(function(){
    async.series([
        function(cb){
            $.Mustache.load('/mustache').done(function(){
                cb();
            });
        }
    ], function(){
        ROUTER = new MainRouter({ el: '#content' });
        Backbone.history.start();        
    });
});
