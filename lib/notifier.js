var cp = require('child_process');

module.exports.notify = function(msg){
    cp.spawn('/usr/bin/notify-send', [ msg ]);
};
