var util = require('util');

var cfg = require('config');
var mongoose = require('mongoose');
var winston = require('winston');

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, { timestamp: true });

winston.info('Connecting to database: %s', cfg.mongodb_url);
mongoose.connect(cfg.mongodb_url);

var Client = mongoose.model('Client', {
    name: String,
    address: String,
    address2: String,
    city: String,
    state: String,
    zip: String,
    phone: String,
    projects: [{
        id: String,
        name: String,
        tasks: [{
            id: String,
            name: String,
            durations: [{
                start: Date,
                end: Date
            }]
        }]
    }]
});

module.exports.Client = Client;
